import argparse
import subprocess
from pathlib import Path


def get_cli_arguments():
    parser = argparse.ArgumentParser(description='Clone repositories')
    parser.add_argument('-rf', '--repo_file', type=Path, default='repositories.txt', help='File with repositories', required=False)
    parser.add_argument('-d', '--repo_dir', type=Path, default='repo_dir', help='Directory for repositories', required=False)
    args = parser.parse_args()
    return args

    
def clone_repositories(repo_file, repo_dir):
    if not repo_file.is_file():
        print(str(repo_file) + ' does not exist!')
        return
    repo_dir.mkdir(parents=True, exist_ok=True)
    with open(args.repo_file) as file:
        for line in file:
            repo_str = line.rstrip('\n')
            subprocess.call(['git', 'clone', repo_str], cwd=repo_dir)


if __name__ == '__main__':

    args = get_cli_arguments()    
    clone_repositories(args.repo_file, args.repo_dir)
