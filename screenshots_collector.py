import argparse
import time
import subprocess
import os
import pygments
import multiprocessing
from pathlib import Path
from pynvim import attach
from abc import ABC, abstractmethod
from pygments import highlight
from pygments.formatters import ImageFormatter
from pygments.lexers import get_lexer_for_filename
from pygments.styles import get_style_by_name, get_all_styles
from itertools import product


SCROT = 'scrot'
PYGMENTS = 'pygments'
SCREENSHOT_TYPES = {SCROT: lambda x: ScreenshotsCollectorWithScrot(x.font_name, x.font_size, x.timeout),
                    PYGMENTS: lambda x: ScreenshotsCollectorWithPygments(x.font_name, x.font_size)}

def get_screenshots_collector(args):
    return SCREENSHOT_TYPES[args.screenshot_type](args)
    
def get_cli_arguments():
    NVIM_COLOR_SCHEMES_NAMES = ['blue', 'darkblue', 'default', 'delek', 'desert', 'elflord', 'evening', 
                                'industry', 'koehler', 'morning', 'murphy', 'pablo', 'peachpuff', 'ron', 
                                'shine', 'slate', 'torte', 'zellner']
    parser = argparse.ArgumentParser(description='Screenshots Collector')
    parser.add_argument('-d', '--repo_dir', type=Path, default='repo_dir', help='Directory with repositories', required=False)
    parser.add_argument('-e', '--file_ending', type=str, default='screenshot', help='Ending for screenshot file', required=False)
    subparsers = parser.add_subparsers(help='Screenshot type', dest='screenshot_type')

    parser_scrot = subparsers.add_parser(SCROT, help=SCROT)
    parser_scrot.add_argument('-t', '--timeout', type=int, default=0.7, help='Timeout for open file in editor (seconds)', required=False)
    parser_scrot.add_argument('-c', '--color_scheme', type=str, default='default', help='Nvim color scheme', \
                              choices=NVIM_COLOR_SCHEMES_NAMES, required=False)
    parser_scrot.add_argument('-f', '--font_name', type=str, default='DejaVu Sans Mono', \
                              help='Kitty font. View all available fonts: kitty list-fonts', required=False)
    parser_scrot.add_argument('-s', '--font_size', type=int, default=14, help='Font size', metavar='[8-97]', choices=range(8, 97), required=False)
    
    parser_pygments = subparsers.add_parser(PYGMENTS, help=PYGMENTS)
    parser_pygments.add_argument('-c', '--color_scheme', type=str, default='colorful', help='Pygments style', \
                                 choices=list(get_all_styles()), required=False)
    parser_pygments.add_argument('-f', '--font_name', type=str, default='DejaVuSansMono', \
                                 help='Pygments font. System-installed fonts are used. View all available fonts: fc-list', required=False)
    parser_pygments.add_argument('-s', '--font_size', type=int, default=14, help='Font size', metavar='[8-97]', choices=range(8, 97), required=False)
    parser_pygments.add_argument('-j', '--process_number', type=int, default=4, help='Process number', required=False)
   
    args = parser.parse_args()
    return args
    

class ScreenshotsCollector:

    @abstractmethod
    def make_screenshots(self, python_files, file_ending, color_scheme):
        pass

    @abstractmethod            
    def close(self):
        pass


class ScreenshotsCollectorWithScrot(ScreenshotsCollector):
    
    def __init__(self, font_name, font_size, timeout):
        super().__init__()
        NVIM_LISTEN_ADDRESS = '/tmp/nvim'
        proc = subprocess.Popen(['kitty', '-o', 'font_size=' + str(font_size), '-o', 'font_family=' + font_name, \
                                 '--start-as', 'fullscreen', '--', 'env', 'NVIM_LISTEN_ADDRESS=' + NVIM_LISTEN_ADDRESS, 'nvim'])
        self.nvim = self._nvim_connector(NVIM_LISTEN_ADDRESS)
        self.timeout = timeout
        self._set_nvim()

    def _nvim_connector(self, nvim_listen_address):
        NVIM_CONNECTION_TIMEOUT = 3
        timeout = time.time() + NVIM_CONNECTION_TIMEOUT 
        while True:
            try:
                return attach('socket', path=nvim_listen_address)
            except:
                if time.time() > timeout:
                    print('Error in connection to nvim!')
                    raise

    def _set_nvim(self):
        self.nvim.command('set noshowmode')
        self.nvim.command('set noruler')
        self.nvim.command('set laststatus=0')
        self.nvim.command('set noshowcmd')
               
    def make_screenshot(self, python_file, file_ending, color_scheme):
        self.nvim.command('colorscheme ' + color_scheme)
        self.nvim.command('edit ' + str(python_file))
        time.sleep(self.timeout)
        subprocess.call(['scrot', '-u', str(python_file) + '.' + file_ending + '.png'])
        self.nvim.command('bd')
    
    def close(self):
        self.nvim.quit()
        self.nvim.close() 


class ScreenshotsCollectorWithPygments(ScreenshotsCollector):
               
    def __init__(self, font_name, font_size):
        super().__init__()
        self.font_name = font_name
        self.font_size = font_size
        
    def make_screenshot(self, python_file, file_ending, color_scheme):
        with open(python_file) as file:
            code = file.read()
            image_formatter = ImageFormatter(line_numbers=False, font_size=self.font_size, \
                                             font_name = self.font_name, style=color_scheme)
            code_img = highlight(code, get_lexer_for_filename(python_file), image_formatter)
            with open(str(python_file) + '.' + file_ending + '.png', 'wb') as out_file:
                out_file.write(code_img)
                
    def wrapper_make_screenshot(self, make_screen_args):
        self.make_screenshot(*make_screen_args)


if __name__ == '__main__':

    args = get_cli_arguments()
    
    python_files = list(args.repo_dir.glob('**/*.py'))
    
    screenshots_collector = get_screenshots_collector(args)
    if args.screenshot_type == PYGMENTS:
        with multiprocessing.Pool(processes=args.process_number) as pool:
            pool.map(screenshots_collector.wrapper_make_screenshot, \
                     [(python_file, args.file_ending, args.color_scheme) for python_file in python_files])
    else:
        [screenshots_collector.make_screenshot(python_file, args.file_ending, args.color_scheme) for python_file in python_files]
    screenshots_collector.close() 
        
  

