import argparse
from pathlib import Path


def get_cli_arguments():
    parser = argparse.ArgumentParser(description='Remove png files')
    parser.add_argument('-d', '--repo_dir', type=Path, default='repo_dir', help='Directory with repositories', required=False)
    args = parser.parse_args()
    return args
    
    
if __name__ == '__main__':

    args = get_cli_arguments()  
    png_files = list(args.repo_dir.glob('**/*.png'))
    for png_file in png_files:
        png_file.unlink()
    
