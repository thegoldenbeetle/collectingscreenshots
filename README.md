# Распознавание кода со скриншотов из различных IDE

Представлена базовая версия скрипта для сбора данных.

## Описание решения

Скрипт для генерации скриншотов - ```screenshots_collector.py```.
В скритпте предусмотрено 2 режима:
- scrot - каждый файл открывается в редакторе neovim и терминале kitty, затем делается скриншот с помощью утилиты scrot.
- pygments - каждый файл открывается, его текст преобразуется в соответствии с указанной цветовой схемой и шрифтом с помощью библиотеки pygments, затем сохраняется в формате png.

Каждый режим допускает настройку шрифта, цветовой схемы, окончания имени файла со скриншотом.

Примеры запуска скрипта:
- `python3 --file_ending 'screenshot' --repo_dir 'repo_dir' screenshots_collector.py scrot --timeout 0.7 --font_size 14 --font_name 'DejaVu Sans Mono' --color_scheme=default`
- `python3 --file_ending 'screenshot' --repo_dir 'repo_dir' screenshots_collector.py pygments --font_size 14 --font_name 'DejaVuSansMono' --color_scheme=colorful --thread_number=4`
- `python3 screenshots_collector.py scrot`
- `python3 screenshots_collector.py pygments`

Описание параметров:
- file_ending - окончание имени файла со скриншотом. Для каждого файла \*.py будет создан файл \*.file_ending.png.
- repo_dir - директория с репозиториями, где находятся файлы с кодом и куда будут сохранены скриншоты.
- timeout - параметр для режима scrot. Задержка в секундах от команды открытия файла до его отображения на экране.
- font_size - размер шрифта.
- font_name - имя шрифта. Для режима scrot используются шрифты из kitty по имени, для pygments - имеющиеся в системе шрифты по имени ttf файла.
- color_scheme - цветовая схема. Для режима scrot используются темы из neovim, для pygments - темы из pygments.
- thread_number - параметр для режима pygments. Запуск кода параллельно, указывается число потоков.

Более подробную справку по параметрам скрипта можно получить, запустив:
- `python3 main.py -h`
- `python3 main.py scrot -h`
- `python3 main.py pygments -h`

Получившиеся скриншоты для режима scrot находятся в папке ```repo_dir_scrot```, для режима pygments - в папке ```repo_dir_pygments```.

## Структура каталога:

- ```screenshots_collector.py``` - скрипт для сбора скриншотов.
- ```remove_png.py``` - скрипт для удаления всех файлов с расширением .png.
- ```clone_repositories.py``` - скрипт для клонирования репозиториев по адресам, указанным в файле repositories.txt.
- ```requirements_apt.txt``` - необходимые утилиты, устанавливаемые через apt.
- ```requirements_pip.txt``` - необходимые библиотеки.
- ```repo_dir_scrot``` - файлы с кодом и соответствующими скриншотами в режиме scrot
- ```repo_dir_pygments``` - файлы с кодом и соответствующими скриншотами в режиме pygments

## Системные требования

- ОС: Ubuntu 20.04
- Python 3.8








